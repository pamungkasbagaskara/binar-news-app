import React, { Component } from 'react';
import { View, FlatList, TouchableHighlight, TextInput } from 'react-native';
import { Text, Card, Divider } from 'react-native-elements';
import { SearchBar } from 'react-native-elements';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import moment from 'moment';


// import Article from './Article';
import fetchHome from '../utils/fetchHome' ;
import { WebView } from 'react-native-webview';
import fetchSearch from '../utils/fetchSearch'

// export class Search extends React.Component{
// 	constructor(props) {
// 		super(props);

//         this.state = {
// 			Search: '',
			
// 		};
// 	}
	
// 	render() {
//         const {navigation} = this.props;

// 		return (
			
			
// 			<View>
// 				<View>
// 				<TextInput
		
// 					placeholder="Search"
// 					placeholderTextColor="#003f5c"
					
// 					onChangeText={(value) => this.setState({Search:value})}
// 					value={this.state.Search}
					
// 				/>
// 				</View>
// 				<TouchableOpacity 
// 				// style={styles.button} 
// 					onPress={()=> {
// 					// alert(`search: ${this.state.Search}`)
// 					// console.log(typeof navigation) ;
// 					// navigation.navigate('Home');
// 					<NewsSearch 
// 						search = {this.state.Search}
// 					/>
					
// 					// this.handleMasuk ({email:this.state.email, password:this.state.password})
// 					// this.getData()
// 					// this.saveData.bind(this)
					
// 					}}>
						
// 					<Text >Search</Text>
// 				</TouchableOpacity>
// 		</View>

// 		)
//     }	
// }

// export default Search;



class Search extends Component {
	
	state = {
		
		articles: [],
		refreshing: true,
		url : '',
		page : 1,
		keyWord :'',
	};

	// componentDidMount = () => {
	// 	this.fetchNews(this.state.page);
	// };

	fetchNews = (number, word) => {
		fetchSearch(number, word)
		.then(articles => {
			this.setState({ articles, refreshing: false });
			console.log(this.state.keyWord)
			})
			.catch(() => this.setState({ refreshing: false }));
	};

	fetchNewPage = (number, word) => {
		fetchSearch(number, word)
			.then(articles => {
				let pageBefore = this.state.articles;
				
				if (articles.length === 0){
					alert ('No More Item')
					this.setState({ articles : pageBefore, refreshing: false });
				} else {
					let pageNew = pageBefore.concat(articles) 
					this.setState({ articles : pageNew, refreshing: false });
				}
			})
			.catch(() => this.setState({ refreshing: false }));		
	}

	handleRefresh = () => {
		this.setState({ refreshing: true }, () => this.fetchNews(this.state.page, this.state.keyWord));
	};
	handleNextPage=() => {
		this.state.page=this.state.page+1
		console.log(this.state.page)
		this.setState({ refreshing: true }, () => this.fetchNewPage(this.state.page, this.state.keyWord));
	}
	
	tekan= (url) => {
		const {navigation} = this.props;
		console.log('Pressed')
		// const navigation = {this.props.navigation}
		navigation.navigate("WebViewScreen", {               //<--- Call WebViewScreen and Pass URL
			url: url
			})
		// navigation.navigate('Search');
		// console.log(typeof props.navigation)
	};

	Article = ({item}) => {
		// class Article extends Component {
		

				const time = moment(item.publishedAt || moment.now()).fromNow();
				
				return (
					<View>
						
					<TouchableHighlight 
						onPress={() =>
							// Linking.openURL(url)
							this.tekan ({url:item.url})
							
						}
			
					>
						{/* <Text >{source.name.toUpperCase()}</Text> */}
						
						<Card>
							<Card.Image source={{
								uri:
									`${item.urlToImage}`
							}}> 
							</Card.Image>
							<Card.Title>{item.title}</Card.Title>
							<Text style={{ marginBottom: 10 }} 
							// onPress={() => navigation.navigate('Search')} 
							>
								{item.description || 'Read more...'}
							</Text>
							<Divider style={{ backgroundColor: '#dfe6e9' }} />
							<View
								style={{ flexDirection: 'row', justifyContent: 'space-between' }}
							>
								<Text
									style={{
										margin: 5,
										fontStyle: 'italic',
										color: '#b2bec3',
										fontSize: 10
									}}
								>
									{item.source.name.toUpperCase()}
								</Text>
								<Text
									style={{
										margin: 5,
										fontStyle: 'italic',
										color: '#b2bec3',
										fontSize: 10
									}}
								>
									{item.time}
								</Text>
							</View>
						</Card> 
					</TouchableHighlight>
					</View>
				);
			
		}
		

	render() {
		// console.log(this.state.articles)
		return (
			<View>
				<SearchBar
					containerStyle={{ backgroundColor :'#e0ffff' }}
					inputContainerStyle={{ backgroundColor :'#f0f8ff' }}
					placeholder="Search Here..."
					// backgroundColor='white'
					onChangeText={(keyWord) => this.setState({keyWord})}
					value={this.state.keyWord}
					onSubmitEditing = {()=> this.handleRefresh()}
				/>
			{/* <View style={{
							flex: 1,
							flexDirection: 'row',
							justifyContent: 'center',
							alignItems: 'center',
							backgroundColor: '#fff',
							paddingTop:40,
							borderRadius: 0,
					  }}>
				<MaterialCommunityIcons name="magnify" style={{
					padding: 10,
				}} />
				<TextInput
				    style={{
						flex: 1,
						paddingTop: 10,
						paddingRight: 10,
						paddingBottom: 10,
						paddingLeft: 0,
						backgroundColor: '#fff',
						color: '#424242',
					  }}
					  inlineImageLeft='search_icon'
					placeholder="Search Here..."
					placeholderTextColor="#003f5c"
					onChangeText={(keyWord) => this.setState({keyWord})}
					value={this.state.keyWord}
					onSubmitEditing = {()=> this.handleRefresh()}
					// onChangeText={(password) => setPassword(password)}
				/>
			</View> */}


			<FlatList
				data={this.state.articles}
				// renderItem={({ item }) => <Article article={item} />}
				renderItem={this.Article}
				keyExtractor={item => item.url}
				refreshing={this.state.refreshing}
				onRefresh={this.handleRefresh}
				onEndReached={this.handleNextPage}
			/>
			</View>
		);
	}
}

export default Search;