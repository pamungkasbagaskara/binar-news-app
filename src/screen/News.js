import React, { Component } from 'react';
import { View, FlatList, TouchableHighlight } from 'react-native';
import { Text, Card, Divider } from 'react-native-elements';

import moment from 'moment';


// import Article from './Article';
import fetchHome from '../utils/fetchHome' ;
import { WebView } from 'react-native-webview';


class News extends Component {
	
	state = {
		
		articles: [],
		refreshing: true,
		url : '',
		page : 1,
	};

	componentDidMount = () => {
		this.fetchNews(this.state.page);
	};

	fetchNews = (number) => {
		fetchHome(number)
			.then(articles => {
				this.setState({ articles, refreshing: false });
			})
			.catch(() => this.setState({ refreshing: false }));
	};

	fetchNewPage = (number) => {
		fetchHome(number)
			.then(articles => {
				let pageBefore = this.state.articles;
				
				if (articles.length === 0){
					alert ('No More Item')
					this.setState({ articles : pageBefore, refreshing: false });
				} else {
					let pageNew = pageBefore.concat(articles) 
					this.setState({ articles : pageNew, refreshing: false });
				}
			})
			.catch(() => this.setState({ refreshing: false }));		
	}

	handleRefresh = () => {
		this.setState({ refreshing: true }, () => this.fetchNews(this.state.page));
	};
	handleNextPage=() => {
		this.state.page=this.state.page+1
		console.log(this.state.page)
		this.setState({ refreshing: true }, () => this.fetchNewPage(this.state.page));
	}
	
	tekan= (url) => {
		const {navigation} = this.props;
		console.log('Pressed')
		// const navigation = {this.props.navigation}
		navigation.navigate("WebViewScreen", {               //<--- Call WebViewScreen and Pass URL
			url: url
			})
		// navigation.navigate('Search');
		// console.log(typeof props.navigation)
	};

	Article = ({item}) => {
		// class Article extends Component {
			const time = moment(item.publishedAt || moment.now()).fromNow();
		return (
			<TouchableHighlight 
				style={{ backgroundColor :"#e0ffff" }}
				onPress={() =>
					// Linking.openURL(url)
					this.tekan ({url:item.url})
				}>
					<Card
						containerStyle={{ backgroundColor :'#f0f8ff' }}>
						
						<Card.Image source={{
							uri:
								`${item.urlToImage}`
							}}> 
						</Card.Image>
						<Card.Title>{item.title}</Card.Title>
						<Text style={{ marginBottom: 10 }}>	
							{item.description || 'Read more...'}
						</Text>
						<Divider style={{ backgroundColor: '#dfe6e9' }} />
						<View
							style={{ flexDirection: 'row', justifyContent: 'space-between' }}
						>
							<Text
								style={{
									margin: 5,
									fontStyle: 'italic',
									color: '#b2bec3',
									fontSize: 10
								}}
							>
								{item.source.name.toUpperCase()}
							</Text>
							<Text
								style={{
									margin: 5,
									fontStyle: 'italic',
									color: '#b2bec3',
									fontSize: 10
								}}
							>
								{item.author}
							</Text>
							<Text
								style={{
									margin: 5,
									fontStyle: 'italic',
									color: '#b2bec3',
									fontSize: 10
								}}
							>
								{time}
							</Text>
						</View>
					</Card> 
			</TouchableHighlight>
		);
	}
		

	render() {
		// console.log(this.state.articles)
		return (
			// <View><Text>tes</Text></View>
			<FlatList
				data={this.state.articles}
				// renderItem={({ item }) => <Article article={item} />}
				renderItem={this.Article}
				keyExtractor={item => item.url}
				refreshing={this.state.refreshing}
				onRefresh={this.handleRefresh}
				onEndReached={this.handleNextPage}
			/>
		);
	}
}

export default News;