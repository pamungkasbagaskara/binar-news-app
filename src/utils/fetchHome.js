import axios from 'axios';
import Config from 'react-native-config';

 async function fetchHome(page) {
        console.log(`${Config.BASE_URL}/top-headlines?country=id&apiKey=${Config.KEY}&page=${page}`);
        const response = await axios.get(`${Config.BASE_URL}/top-headlines?country=id&apiKey=${Config.KEY}&page=${page}`)

        return response.data.articles;

}
export default fetchHome;
