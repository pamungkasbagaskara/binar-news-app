import axios from 'axios';
import Config from 'react-native-config';


 async function fetchSearch(page, keyWord) {
   
    // try{
        // const response = await fetchAxios(`top-headlines?country=id&apiKey=${API_KEY}&page=1`)
        // console.log(page, keyWord)
        const response = await axios.get(`${Config.BASE_URL}/everything?q=${keyWord}&page=${page}&apiKey=${Config.KEY}`)
     
        console.log('res ==', response.data.articles);
        // console.log(typeof response);
        return response.data.articles;


}
export default fetchSearch;
