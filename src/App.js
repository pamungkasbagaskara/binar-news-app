import * as React from 'react';
import {View, Text} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Homescreen from './screen/Homescreen';
import Search from './screen/Search';
import WebViewScreen from "./screen/webViewScreen";
import Article from './components/Article';
import News from './screen/News';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
function TabNavigation() {
	return (
    // <NavigationContainer independent={true}>
    <Tab.Navigator initialRouteName="Home"  >
        <Tab.Screen 
                name="Home"
                component={News}
                options={{
                  tabBarLabel: 'Home',
                  tabBarIcon: ({ color, size }) => (
                    <MaterialCommunityIcons name="home" color={color} size={size} />
                  ),
                }}
        />
        <Tab.Screen
                name="Search"
                component={Search}
                options={{
                tabBarLabel: 'Search',
                tabBarIcon: ({ color, size }) => (
                    <MaterialCommunityIcons name="magnify" color={color} size={size} />
                ),
                }}
        />
	  </Tab.Navigator>
    //  </NavigationContainer>
	);
  }


const Navigation = () => {
    return(
        <NavigationContainer independent={true}>
            <Stack.Navigator initialRouteName="Home" headerMode="none" >
              <Stack.Screen name="Home" component={TabNavigation} />
              <Stack.Screen name="Search" component={Search} />
              <Stack.Screen name="WebViewScreen" component={WebViewScreen} />
              <Stack.Screen name="Article" component={Article} />
            </Stack.Navigator>
      </NavigationContainer>
    )
}
export default Navigation; 

