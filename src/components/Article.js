import React, { Component } from 'react';
import { View, TouchableHighlight, Linking } from 'react-native';
import { Text, Card, Divider } from 'react-native-elements';
import moment from 'moment';
import WebView from 'react-native-webview'

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';


class Article extends Component {
	tekan= (url) => {
		const {navigation} = this.props;
		console.log('Pressed')
		// const navigation = {this.props.navigation}
		navigation.navigate("WebViewScreen", {               //<--- Call WebViewScreen and Pass URL
			url: url
			})
		// navigation.navigate('Search');
		// console.log(typeof props.navigation)
	};
	render() {
		const {
			title,
			description,
			publishedAt,
			source,
			urlToImage,
			url
		} = this.props.article;
		
		
		 	

		const time = moment(publishedAt || moment.now()).fromNow();
		


		return (
			
			
			<TouchableHighlight 
				onPress={() =>
					// Linking.openURL(url)
					this.tekan (url)
					
				}
	
			>
				{/* <Text >{source.name.toUpperCase()}</Text> */}
				
                <Card>
                    <Card.Image source={{
						uri:
							`${urlToImage}`
					}}> 
                    </Card.Image>
                    <Card.Title>{title}</Card.Title>
					<Text style={{ marginBottom: 10 }} 
					// onPress={() => navigation.navigate('Search')} 
					>
						{description || 'Read more...'}
					</Text>
					<Divider style={{ backgroundColor: '#dfe6e9' }} />
					<View
						style={{ flexDirection: 'row', justifyContent: 'space-between' }}
					>
						<Text
							style={{
								margin: 5,
								fontStyle: 'italic',
								color: '#b2bec3',
								fontSize: 10
							}}
						>
							{source.name.toUpperCase()}
						</Text>
						<Text
							style={{
								margin: 5,
								fontStyle: 'italic',
								color: '#b2bec3',
								fontSize: 10
							}}
						>
							{time}
						</Text>
					</View>
				</Card> 
			</TouchableHighlight>
			
		);
	}
}

export default Article;